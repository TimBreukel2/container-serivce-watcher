import logging
import os
import sys
import yaml

from kubernetes import client, config, watch


# Configure logging to output to stdout
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format='%(asctime)s %(levelname)s: %(message)s')

try:
    config.load_incluster_config()
except config.ConfigException:
    config.load_kubeconfig()

api = client.CoreV1Api()
networking = client.NetworkingV1Api()

# Read the network policy YAML definition from a file
with open('/app/network-policy.yaml', 'r') as f:
    policy_yaml = f.read()

# Create a network policy object from the YAML definition
policy = yaml.safe_load(policy_yaml)

# Watch for namespace events
w = watch.Watch()
for event in w.stream(api.list_namespace, timeout_seconds=0):
    namespace_name = event['object'].metadata.name
    if event['type'] == 'ADDED' and 'chal' in namespace_name:
        logging.info(f"Creating network policy for namespace {namespace_name}")
        try:
            networking.create_namespaced_network_policy(namespace=namespace_name, body=policy)
        except client.exceptions.ApiException as e:
            if e.status == 409 and 'already exists' in e.body:
                logging.info(f"Network policy for namespace {namespace_name} already exists, skipping")
            else:
                raise e
    elif event['type'] == 'DELETED' and 'chal' in namespace_name:
        logging.info(f"Deleting network policy for namespace {namespace_name}")
        networking.delete_namespaced_network_policy(namespace=namespace_name, name=policy['metadata']['name'])
