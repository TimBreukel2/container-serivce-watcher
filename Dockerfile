FROM python:3.9

WORKDIR /app

COPY requirements.txt .
COPY network-policy.yaml .
RUN pip install --no-cache-dir -r requirements.txt

COPY namespace_watcher.py .

CMD ["python", "namespace_watcher.py"]