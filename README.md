# Container serivce watcher
Dit is een publieke repo die gebruikt kan worden om namespaces to controleren beginnend met de naam "chal...". 
Wanneer deze namespace aanwezig is zal door de service controller een policy opgelegd worden waarbij het niet mogelijk is om naar buiten te communiceren.
Alleen poort 53 TCP/UDP staat aan om DNS-resolvement te kunnen uitvoeren.

# Done
- Werking getest
- Uitgerold in Kubernetes cluster
- Namepsace werking getest

# To do
- Testen via ArgoCD in AKS cluster

## 1. Prerequisites
Voordat de Container Service Watcher uitgerold kan worden. Moet de Kubernetes-cluster eerst voorbereidt worden.
In deze voorbereiding zullen rollen aangemaakt worden, zodat alles correct uitgevoerd kan worden.

### 1.1 Cluster voorbereiden
1. Maak een service account aan in de Kubernetes cluster doormiddel van het commando:
**Linux**:
```Bash
kubectl create serviceaccount network-policy-service-account
```
2. Navigeer vervolgens naar de map **/Preperations** en vind het bestand **Policywatcher.yml**
3. Typ vervolgens onderstaande commando in om de ClusterRole te importeren.
**Linux**:
```Bash
kubectl apply -f Policywatcher.yml
```
4. Vervolgens in dezelfde map mag er een Service Account aangemaakt worden. Hiervoor wordt het bestand **Policyservicewatcher.yml** gebruikt.
5. Gebruiker hiervoor het commando:
**Linux**:
```Bash
kubectl apply -f Policyservicewatcher.yml
```

## 2. Deployment uitvoeren
Nu de Kubernetes cluster is voorbereid kan de Container Serivce Watcher uitgerold worden. Hiervoor zijn 2 mogelijkheden aanwezig. De deployment via Kubernetes zelf of
een deployment vanuit Argo-CD. Beide opties zijn mogelijk binnen deze repo.

Hieronder staat toegelicht welke stappen uitgevoerd moeten worden om dit te voltooien.

### 2.1 Uitrollen van Container Service Watcher via Docker Image
1. Maak een Docker Image van de huidige repo via het commando:
**Linux**:
```Bash
docker build -t <YOUR_REGISTRY>/<YOUR_IMAGE_NAME>:latest. 
```
2. Wanneer deze ook online benaderbaar moet zijn kan het onderstaande commando uitgevoerd worden:
**Linux**:
```Bash
docker push <YOUR_REGISTRY>/<YOUR_IMAGE_NAME>:latest
```
3. Open nu het bestand **/Preperations/Policycontrollerdeploy.yml** en pas **regel 18** aan.
Verander:
```yaml
image: <image>
```
Naar:
```yaml
image: <YOUR_REGISTRY>/<YOUR_IMAGE_NAME>:latest
```
4. Voer volgens de deployment uit via het commando:
**Linux**
```Bash
kubectl apply -f Policycontrollerdeploy.yml
```
5. Controleer vervolgens of de POD gestart is via:
**Linux**
```Bash
kubectl get pods
```
Als het goed is moet er 1 pod aangemaakt zijn onder de naam **network-policy-controller**


### 2.2 Uitrollen van Container Service Watcher via Gitlab Container Registry
1. Maak een GitLab Container image door een fork te maken van deze repo.
2. Eenmaal geforked. Navigeer naar **Package and registries > Container Registry**
3. Hierin staan voorbeeld commando's beschreven die ook hieronder teruggevonden kunnen worden.
4. Zorg ervoor dat de GitLab repo lokaal aanwezig is op de machine.
5. Open in de lokale repo folder je CLI en gebruik het volgende commando:
**Linux**
```Bash
docker login registry.gitlab.com
```
Login met je GitLab credenties.
6. Bouw vervolgens een Docker container via het commando:
**Linux**
```Bash
docker build -t registry.gitlab.com/timbreukel2/container-serivce-watcher .
```

De registry naam kan teruggevonden worden in het menu van **Package and registries > Container Registry** voor eigen gebruik.

7. Tot slot. Push de Docker Container naar de GitLab repository via:
**Linux**
```Bash
docker push registry.gitlab.com/timbreukel2/container-serivce-watcher
```
8. Open nu het bestand **/Preperations/Policycontrollerdeploy.yml** en pas **regel 18** aan.
Verander:
```yaml
image: <image>
```
Naar:
```yaml
image: registry.gitlab.com/timbreukel2/container-serivce-watcher
```

## 3. Werking testen
Om te controleren of de policy correct toegepast wordt kunnen de volgende stappen doorlopen worden:
1. Maak een namespace met de naam **chal-test**:
**Linux**
```Bash
kubectl create namespace chal-test
```
2. Controleer vervolgens of de network policy **deny-all-except-dns** toegepast wordt op de namespace.
**Linux**
```Bash
kubectl get networkpolicies -n chal-test 
```
3. Vervolgens kan er een test container aangemaakt worden om de werking rondom connectiviteit te testen.
4. Ga hiervoor naar **/Testing** en voer het volgende commando uit:
**Linux**
```Bash
kubectl apply -f Porttest.yml
```
5. Maak vervolgens een tweede namespace aan onder de naam **chal-test2**
6. Hierin gaan wij een Nginx container uitrollen. Voer hiervoor het commando uit:
**Linux**
```Bash
kubectl apply -f Nginx.yml
```
7. Maak vervolgens vanuit de CLI verbinding met de Minikali POD. Hiervoor wordt gebruikt:
**Linux**
```Bash
kubectl exec --stdin --tty -n chal-test <minikali pod> -- nmap -sV <IP>
```
Hierin kunnen enkele testen uitgevoerd worden om de verbinding te testen. Voorbeeld hiervan is:

```Bash
kubectl exec --stdin --tty -n chal-test <minikali pod> -- nmap -sV 8.8.8.8
```

8. Voor het testen van eventuele webpagina kan er ook een curl container uitgerold worden. Dit kan gemakkelijk gedaan worden via het commando:
(Voeg -n <namespace> toe wanneer de curl pod in een namespace geplaatst moet worden.)
**Linux**
```Bash
kubectl run mycurlpod --image=curlimages/curl -i --tty -- sh
```

## 4. Wijzigen van network-policy.yaml
De policies die op een namespace uitgerold worden zijn afkomstig vanuit het bestand network-policy.yaml. In sommige gevallen
komt het voor dat er een netwerk wijziging is en dat dit verwerkt moet worden in de policy om alles te laten werken.

Hieronder staat beschreven welke procedure ervoor aangehouden moet worden.

1. Maak een nieuwe branch aan zoals **Development**, zodat de **main** repo stabiel blijft.
2. Open vervolgens het bestand **network-policy.yaml**
3. Op basis van de netwerk wijzigingen, verander deze wijzigingen in het bestand (Kubernetes kennis vereisd)
4. Doorloop vervolgens de stappen beschreven in **2.1** of **2.2** om deze te laten uitrollen in het cluster.
5. Belangrijk is dat alle Challenge Containers op het moment van wijziging uitgeschakeld + verwijderd zijn! Anders zullen de wijzigingen niet toegepast worden.
6. Eenmaal uitgerold kunnen de Challenge Containers weer uitgerold worden en getest worden beschreven in **3. Werking testen**
