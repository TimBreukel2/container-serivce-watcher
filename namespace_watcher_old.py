import os
import time
from kubernetes import client, config, watch

# Load the in-cluster configuration or fallback to kubeconfig
try:
    config.load_incluster_config()
except config.ConfigException:
    config.load_kubeconfig()

v1 = client.CoreV1Api()
networking_v1 = client.NetworkingV1Api()

def network_policy_exists(namespace, policy_name):
    try:
        network_policies = networking_v1.list_namespaced_network_policy(namespace)
        for policy in network_policies.items:
            if policy.metadata.name == policy_name:
                return True
        return False
    except client.exceptions.ApiException as e:
        print(f"Error listing network policies in namespace {namespace}: {e}")
        return False

def create_or_update_network_policy(namespace):
    network_policy_name = "policy-deny-all-traffic"

    if network_policy_exists(namespace, network_policy_name):
        print(f"Network policy {network_policy_name} already exists in namespace {namespace}. Skipping.")
        return

    # Define the network policy
    policy = client.V1NetworkPolicy(
        metadata=client.V1ObjectMeta(
            name=network_policy_name,
            namespace=namespace
        ),
        spec=client.V1NetworkPolicySpec(
            pod_selector=client.V1LabelSelector(),  # Applies to all pods in the namespace
            policy_types=["Egress"],  # Block all egress traffic
            egress=[
                client.V1NetworkPolicyEgressRule(
                    to=[
                        client.V1NetworkPolicyPeer(
                            namespace_selector=client.V1LabelSelector(
                                match_labels={"kubernetes.io/metadata.name": "kube-system"}
                            ),
                            pod_selector=client.V1LabelSelector(
                                match_labels={"k8s-app": "kube-dns"}  # Allow egress traffic to kube-dns server on port 53
                            ),
                        ),
                    ],
                    ports=[
                        client.V1NetworkPolicyPort(
                            protocol="UDP",
                            port=53
                        ),
                        client.V1NetworkPolicyPort(
                            protocol="TCP",
                            port=53
                        ),
                    ]
                ),
                client.V1NetworkPolicyEgressRule(
                    to=[
                        client.V1NetworkPolicyPeer(
                            pod_selector=client.V1LabelSelector()  # Allow egress traffic to other pods in the namespace
                        ),
                    ],
                ),
            ],
        ),
    )

    try:
        networking_v1.create_namespaced_network_policy(namespace, policy)
        print(f"Network policy {network_policy_name} created in namespace {namespace}")
    except client.exceptions.ApiException as e:
        print(f"Error creating network policy in namespace {namespace}: {e}")




def watch_namespaces():
    while True:
        stream = watch.Watch().stream(v1.list_namespace)
        for event in stream:
            namespace_name = event['object'].metadata.name
            if event['type'] == "ADDED" and namespace_name.startswith("chal-"):
                create_or_update_network_policy(namespace_name)

if __name__ == "__main__":
    watch_namespaces()
    